var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var handlebars = require('gulp-handlebars');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var babelify = require('babelify');
var watchify = require('watchify');

gulp.task('default', plugins.sequence('clean', 'build', 'watch'));

gulp.task('clean', function () {  
    return gulp.src('build', {read: false})
        .pipe(plugins.clean());
});

function compileHtml(cwd, destFolder, destFileName) {
	return gulp.src('**/*.handlebars', {cwd: cwd})
	.pipe(
		handlebars()
	)
	.pipe(plugins.wrap('Handlebars.template(<%= contents %>)'))
	.pipe(
		plugins.declare({
			namespace: 'templates',
			noRedeclare: true,
			root: 'Handlebars' //для чего эта опция? 
		})
	)
	.pipe(plugins.concat(destFileName))
	//.pipe(plugins.uglify())
	.pipe(gulp.dest(destFolder));
}

gulp.task('compile-html', function() {
	compileHtml('src/content', 'build/content/html', 'content_tmpl.js');
	compileHtml('src/popup', 'build/popup/html', 'popup_tmpl.js');
});

gulp.task('less', function () {
	//content.less must import other deps
	return gulp.src('./src/**/content.less')
		.pipe(plugins.less())
		.pipe(plugins.minifyCss())
		.pipe(gulp.dest('./build/'));
});

function compileJS(watch, cwd, destFolder, destFileName) {
    var bundler = watchify(browserify(cwd, { debug: true }).transform(babelify));

    function rebundle() {
        bundler.bundle()
        .on('error', function(err) { console.error(err); this.emit('end'); })
        .pipe(source(destFileName))
        .pipe(buffer())
        .pipe(plugins.sourcemaps.init({ loadMaps: true }))
        .pipe(plugins.sourcemaps.write('./'))
        .pipe(gulp.dest(destFolder));
    }

    if (watch) {
        bundler.on('update', function() {
            console.log('-> bundling...');
            rebundle(destFolder);
        });
    }

    rebundle();
}

gulp.task('moveLibs', function() {
    return gulp.src('src/libs/*.*')
        .pipe(gulp.dest('build/libs/'));
});

gulp.task('manifest', function() {
    return gulp.src(['src/manifest.json', 'src/128.png'])
        .pipe(gulp.dest('build/'));
});

gulp.task('moveFiles', ['manifest', 'moveLibs']);

gulp.task('build', ['compile-html', 'moveFiles', 'less']);

gulp.task('watch', function () {
	gulp.watch('./**/*.less', ['less']);
	compileJS(true, './src/background/background.js', './build/background/', 'background.js');
	compileJS(true, './src/content/content.js', './build/content/', 'content.js');
});
