const Backbone = require('backbone');

let Application = Backbone.Model.extend({
	defaults: {
		sitesList: {},
		updated: {}
	},
	getRequest(url) {
		return new Promise((resolve, reject) => {
			let xhr = new XMLHttpRequest();
			
			xhr.open('GET', url);
			xhr.onload = (event) => {
				const that = event.target;
				if (that.status === 200) {
					resolve(that.response);
				} else {
					let error = new Error(that.statusText);
					error.code = that.status;
					reject(error);
				}
			};
			xhr.onerror = () => {
				reject(new Error('Network Error'));
			};
			xhr.send();
		});
	},
	initialize() {
		const updatedTime = Date.parse(localStorage.getItem('updated'));
		const currentTime = Date.now();
		this.on('change', this.saveList);
		
		if (!updatedTime || currentTime - updatedTime > 3600000) {
			this.loadList().then((result) => {
				this.set('sitesList', result);
			});
			this.set('updated', Date());
		} else {
			this.set({
				sitesList: localStorage.getItem('sitesList'),
				updated: localStorage.getItem('updated')
			});
		}
	},
	loadList() {
		return this.getRequest('http://www.softomate.net/ext/employees/list.json');
	},
	saveList() {
		localStorage.setItem('sitesList', this.get('sitesList'));
		localStorage.setItem('updated', this.get('updated'));
	}
});

module.exports = Application;
