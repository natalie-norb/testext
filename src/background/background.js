const Application = require('./application');

const application = new Application();

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	if (request.method == "getList") {
		sendResponse({sitesList: localStorage['sitesList']});
	} else {
		sendResponse({}); // snub them.
	}
});

