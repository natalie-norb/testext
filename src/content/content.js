const siteName = location.hostname;

function showMessage(msg) {
	const body = document.querySelector('body');
	let msgBlock = document.createElement('div');
	
	msgBlock.innerHTML = Handlebars.templates.content(msg);
	body.appendChild(msgBlock);
}

chrome.runtime.sendMessage({method: 'getList'}, (response) => {
	let sitesList = JSON.parse(response.sitesList);
	
	sitesList.forEach((site) => {
		if (siteName.indexOf(site.name) !== -1) {
			showMessage(site);
		} else {
			console.log('site in list: %s', site.name);
		}
	});
});
